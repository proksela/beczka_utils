import os
import argparse
import imghdr
import collections
from pathlib import Path
from shutil import copyfile
from PIL import Image, ImageOps # pip3 install Pillow


def file_check(file):
	return file.is_file() and file.suffix in ('.jpg', '.jpeg', '.JPG', '.JPEG')

def prepare_file_name(basename, counter, extension):
	return basename + '_' + str(counter).zfill(4) + extension

def file_sorting_base(file):
	return file.stat().st_mtime

def main():
	
	# image defaults
	DEFAULT_SIZE = 900
	THUMBS_SIZE = 80
	COVER_SIZE = 100
	DEFAULT_QUALITY = 95

	# argument processing
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('input', help='input images location')
	parser.add_argument('cover', help='file used as the album cover')
	parser.add_argument('output', help='output content location')
	parser.add_argument('--verbose', help='output content location', action='store_const', const=True)
	args = parser.parse_args()
	input_path = Path(args.input)
	output_path = Path(args.output)
	verbose = args.verbose

	# validations

	## input directory must exist
	if not input_path.exists():
		parser.print_usage()
		parser.exit(message="no such input directory: {}\n".format(input_path))

	## output directory mustn't already exists and its parent directory must exist
	try:
		output_path.mkdir(parents=False)
	except FileNotFoundError as e:
		parser.print_usage()
		parser.exit(message="output directory parent doesn't exist: {}\n".format(output_path.parent))
	except FileExistsError as e:
		parser.print_usage()
		parser.exit(message="output directory already exists: {}\n".format(output_path))

	# creating photos and thumbs directories
	photos_pathname, thumbs_pathname = args.output + '/photos', args.output + '/thumbs'
	photos_path, thumbs_path = Path(photos_pathname), Path(thumbs_pathname)
	photos_path.mkdir(parents=True)
	thumbs_path.mkdir(parents=True)

	output_basename = os.path.basename(args.output)
	if verbose:
		print("processing album {}".format(output_basename))

	# creating cover image file
	if args.cover:
		cover_path = Path(args.cover)
		image = Image.open(cover_path)
		image = ImageOps.fit(image, (COVER_SIZE, COVER_SIZE))
		image.save(args.output + '/cover.jpg', "JPEG", quality=DEFAULT_QUALITY)
		if verbose:
			print('[{}] {} -> {}'.format(str(0).zfill(4), str(cover_path), args.output + '/cover.jpg'))

	# preparing files for sorting
	dir_unordered_files = map((lambda file: (file_sorting_base(file), file)), input_path.iterdir())

	file_counter = 1
	for _, file in sorted(dir_unordered_files):
		if file_check(file):
			# building photo and thumb files 
			extension = file.suffix
			file_name = prepare_file_name(output_basename, file_counter, extension)
			photos_dst, thumbs_dst = photos_pathname + '/' + file_name, thumbs_pathname + '/' + file_name
			if verbose:
				print('[{}] {} -> {}'.format(str(file_counter).zfill(4), str(file), photos_dst))
			file_counter += 1
			# scaling and saving photo image
			image = Image.open(str(file))
			image_width, image_height = image.size
			if image_width > image_height:
				ratio = image_width / DEFAULT_SIZE
				target_height = image_height / ratio
				image = image.resize((DEFAULT_SIZE, int(target_height)), Image.ANTIALIAS)
			else:
				ratio = image_height / DEFAULT_SIZE
				target_width = image_width / ratio
				image = image.resize((int(target_width), DEFAULT_SIZE), Image.ANTIALIAS)
			image.save(photos_dst, "JPEG", quality=DEFAULT_QUALITY)
			# scaling and saving thumb image
			image = ImageOps.fit(image, (THUMBS_SIZE, THUMBS_SIZE))
			image.save(thumbs_dst, "JPEG")


if __name__ == '__main__':
	main()